#!/bin/bash

FOLDER_PATH=$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")
BACKUP_PATH=$(dirname "$FOLDER_PATH")

# COPY
cp -rfv "$FOLDER_PATH" "$BACKUP_PATH/backup"

# REDUCE SIZE
rm -rfv "$BACKUP_PATH/backup/application/public/build"
rm -rfv "$BACKUP_PATH/backup/application/vendor"
rm -rfv "$BACKUP_PATH/backup/application/node_modules"
rm -rfv "$BACKUP_PATH/backup/application/var/log"
rm -rfv "$BACKUP_PATH/backup/application/var/cache"

# COMPRESS
tar -czf "$BACKUP_PATH/backup.tar.bz2" "$BACKUP_PATH/backup"

# CLEANUP
rm -rfv "$BACKUP_PATH/backup"

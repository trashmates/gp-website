##
## Git Managment
## -------
##

install-dev: ## Install project
	cp .docker/nginx/conf.d/default.conf.dev.dist .docker/nginx/conf.d/default.conf
	cp .docker/mysql/conf.d/my.cnf.dist .docker/mysql/conf.d/my.cnf
	cp .docker/docker-compose.yml.dist .docker/docker-compose.yml

install-prod: ## Install project
	cp .docker/nginx/conf.d/default.conf.dist .docker/nginx/conf.d/default.conf
	cp .docker/mysql/conf.d/my.cnf.dist .docker/mysql/conf.d/my.cnf
	cp .docker/docker-compose.yml.dist .docker/docker-compose.yml

backup:
	chmod +x ./scripts/backup.sh
	./scripts/backup.sh

##
## Docker Management
## -------
##
up: ## Start docker
	docker-compose -f .docker/docker-compose.yml up --build

up-d: ## Start docker in the background
	docker-compose -f .docker/docker-compose.yml up --build -d

down: ## Stop docker
	docker-compose -f .docker/docker-compose.yml down

kill: ## kill docker container
	docker-compose -f .docker/docker-compose.yml kill
	docker-compose -f .docker/docker-compose.yml down --volumes --remove-orphans

ssh-nginx: ## SSH into nginx container
	docker exec -it nginx /bin/sh

ssh-php: ## SSH into PHP container
	docker exec -it php /bin/sh

ssh-database:: ## SSH into MySQL container
	docker exec -it database mysql

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

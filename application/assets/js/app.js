// GREENPEAS - Module Loader

// S1. GLOBAL CSS
require('../css/lib/bootstrap.min.css');
require('../css/lib/mdb.min.css');
require('../css/lib/datatable.min.css');
require('../css/app.sass');
require('select2/src/scss/core.scss');

// S2. GLOBAL JS
const $ = require('jquery');
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
require('../js/lib/bootstrap.bundle.min');
require('../js/lib/datatable.min');
require('../js/lib/tinymce/tinymce.min');
require('select2');

if (document.querySelector('textarea')) {
    tinymce.suffix = '.min';
    tinyMCE.init({
        mode : "textareas",
        plugins: 'print preview fullpage paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        menubar: 'file edit view insert format tools table help',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media link anchor codesample | ltr rtl',
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: "30s",
        autosave_prefix: "{path}{query}-{id}-",
        autosave_restore_when_empty: false,
        autosave_retention: "2m",
        height: '500px'
    });
}

if (document.querySelector('table')) {
    $('table').DataTable();
}

if (document.querySelectorAll('.select2')) {
    $('.select2').select2();
}

// GREENPEAS - Module Loader

// S1. GLOBAL CSS
import '../css/lib/bootstrap.min.css';
import '../css/lib/font-awesome.min.css';
import '../css/lib/agency.min.css';
// require('../css/app.sass');

// S2. GLOBAL JS
var $ = require('jquery');
global.$ = $;
global.jQuery = $;

import '../js/lib/bootstrap.bundle.min';
import '../js/lib/jquery.easing.min.js';
import '../js/lib/agency.min.js';

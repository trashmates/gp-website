<?php

namespace App\Manager;

use App\Entity\Entry;
use App\Entity\File;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;

class EntryManager
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(Security $security, ObjectManager $manager)
    {
        $this->security = $security;
        $this->manager = $manager;
    }

    /**
     * @param FormInterface $form
     * @param Entry         $entry
     *
     * @return bool
     *
     * @throws Exception
     */
    public function processForm(FormInterface $form, Entry $entry): bool
    {
        if ($form->isSubmitted()) {
            $this->validateForm($form);
            if ($form->isValid()) {
                $entry = $this->handleUploadedFiles($form, $entry);

                $this->manager->persist($entry);
                $this->manager->flush();

                return true;
            }
        }

        return false;
    }

    /**
     * @param Entry $entry
     *
     * @return bool
     *
     * @throws Exception
     */
    public function removeEntry(Entry $entry): bool
    {
        foreach ($entry->getFiles() as $file) {
            $file->setDeletedAt(new DateTime());
            $this->manager->persist($file);
        }

        if (null !== $entry->getThumbnail()) {
            $entry->getThumbnail()->setDeletedAt(new DateTime());
            $this->manager->persist($entry->getThumbnail());
        }

        $this->manager->remove($entry);
        $this->manager->flush();

        return true;
    }

    /**
     * @param FormInterface $form
     * @param Entry         $entry
     *
     * @return Entry
     *
     * @throws Exception
     */
    private function handleUploadedFiles(FormInterface $form, Entry $entry): Entry
    {
        if (null !== $form->get('files')) {
            foreach ($form->get('files') as $uploadedFile) {
                /** @var File $file */
                $file = $uploadedFile->getData();

                if (null === $file->getId() && null !== $file->getFile()) {
                    $file->setName($file->getFile()->getClientOriginalName());
                    $entry->addFile($file);
                }

                $this->manager->persist($file);
            }
        }

        if (null !== $form->get('thumbnail')->getData()) {
            $file = new File();
            $file
                ->setPublished(true)
                ->setDescription('Thumbnail - '.$entry->getTitle())
                ->setName('Thumbnail - '.$entry->getTitle())
                ->setFile($form->get('thumbnail')->getData());

            $this->manager->persist($file);
            $entry->setThumbnail($file);
        }

        $this->manager->flush();

        return $entry;
    }

    /**
     * @param FormInterface $form
     */
    private function validateForm(FormInterface $form)
    {
        /** @var File[] $files */
        $files = $form->get('files')->getData();
        foreach ($files as $file) {
            if (null === $file->getId() && null === $file->getFile()) {
                $form->addError(new FormError('Erreur lors de l\'envoi des fichiers !'));
            }
        }
    }
}

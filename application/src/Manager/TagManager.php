<?php

namespace App\Manager;

use App\Entity\Tag;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class TagManager
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Processes the submitted form, and saves all modifications.
     *
     * @param FormInterface $form
     * @param Request       $request
     *
     * @return bool
     */
    public function processForm(FormInterface $form, Request $request): bool
    {
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->saveEntity($form->getData());

            return true;
        }

        return false;
    }

    /**
     * Removes a tag from the database.
     *
     * @param Tag $tag
     */
    public function remove(Tag $tag)
    {
        $this->objectManager->remove($tag);
        $this->objectManager->flush();
    }

    /**
     * Persists and flushes the tag.
     *
     * @param Tag $tag
     */
    private function saveEntity(Tag $tag)
    {
        $this->objectManager->persist($tag);
        $this->objectManager->flush();
    }
}

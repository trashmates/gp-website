<?php

namespace App\Form;

use App\Entity\Entry;
use App\Repository\EntryTypeRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\Valid;

class EntryType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('published')
            ->add('thumbnail', FileType::class, [
                'mapped' => false,
            ])
            ->add('title', null, [
                'label' => 'Titre',
            ])
            ->add('description')
            ->add('type', null, [
                'choice_label' => 'name',
                'query_builder' => static function (EntryTypeRepository $entryTypeRepository) {
                    return $entryTypeRepository->createQueryBuilder('et')
                        ->orderBy('et.name')
                    ;
                },
            ])
            ->add('content', null, [
                'label' => 'Contenu',
                'attr' => ['class' => 'ckeditor'],
            ])
            ->add('files', CollectionType::class, [
                'label' => false,
                'entry_type' => FileUploadType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_file_upload' => true,
                'by_reference' => false,
                'constraints' => [new Valid()],
            ])
            ->add('createdAt', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de création',
            ])
            ->add('user', null, [
                'query_builder' => function (UserRepository $userRepository) {
                    return $userRepository->createQueryBuilder('u')
                        ->orderBy('u.firstname, u.lastname')
                    ;
                },
            ])
            ->add('tags', null, [
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'select2',
                ],
                'query_builder' => function (TagRepository $tagRepository) {
                    return $tagRepository->createQueryBuilder('t')
                        ->orderBy('t.name')
                    ;
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entry::class,
        ]);
    }
}

<?php

namespace App\Entity;

use App\Entity\Traits\BlameableTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 * @Gedmo\Loggable
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class Contact
{
    use BlameableTrait;
    use TimestampableTrait;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     * @Gedmo\Versioned()
     */
    private $published;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned()
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned()
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned()
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Versioned()
     */
    private $picturePath;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned()
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $work;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned()
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $contribution;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Entry", mappedBy="contacts")
     */
    private $entries;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\File", mappedBy="contacts")
     */
    private $files;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    protected $deletedAt;

    public function __construct()
    {
        $this->entries = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool|null
     */
    public function getPublished(): ?bool
    {
        return $this->published;
    }

    /**
     * @param bool $published
     *
     * @return $this
     */
    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return $this
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return $this
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPicturePath(): ?string
    {
        return $this->picturePath;
    }

    /**
     * @param string|null $picturePath
     *
     * @return $this
     */
    public function setPicturePath(?string $picturePath): self
    {
        $this->picturePath = $picturePath;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWork(): ?string
    {
        return $this->work;
    }

    /**
     * @param string $work
     *
     * @return $this
     */
    public function setWork(string $work): self
    {
        $this->work = $work;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContribution(): ?string
    {
        return $this->contribution;
    }

    /**
     * @param string $contribution
     *
     * @return $this
     */
    public function setContribution(string $contribution): self
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * @return Collection|Entry[]
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    /**
     * @param Entry $entry
     *
     * @return $this
     */
    public function addEntry(Entry $entry): self
    {
        if (!$this->entries->contains($entry)) {
            $this->entries[] = $entry;
            $entry->addContact($this);
        }

        return $this;
    }

    /**
     * @param Entry $entry
     *
     * @return $this
     */
    public function removeEntry(Entry $entry): self
    {
        if ($this->entries->contains($entry)) {
            $this->entries->removeElement($entry);
            $entry->removeContact($this);
        }

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @param File $file
     *
     * @return $this
     */
    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->addContact($this);
        }

        return $this;
    }

    /**
     * @param File $file
     *
     * @return $this
     */
    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            $file->removeContact($this);
        }

        return $this;
    }
}

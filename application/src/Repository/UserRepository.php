<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $token
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getOneByToken(string $token)
    {
        return $this->getByTokenQueryBuilder($token)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param string $token
     *
     * @return QueryBuilder
     */
    public function getByTokenQueryBuilder(string $token): QueryBuilder
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.resetToken = :resetToken')
            ->setParameter('resetToken', $token)
        ;
    }
}

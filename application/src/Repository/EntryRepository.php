<?php

namespace App\Repository;

use App\Entity\Entry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Entry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entry[]    findAll()
 * @method Entry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entry::class);
    }

    /**
     * @param bool   $published
     * @param string $type
     *
     * @return QueryBuilder
     */
    public function getPublishedEntriesQueryBuilder(bool $published, string $type): QueryBuilder
    {
        return $this->createQueryBuilder('entry')
            ->join('entry.type', 'type')
            ->andWhere('entry.published = :published')
            ->andWhere('type.name = :type')
            ->orderBy('entry.createdAt', 'DESC')
            ->setParameters([
                'published' => $published,
                'type' => $type,
            ])
        ;
    }

    /**
     * @param bool   $publushed
     * @param string $type
     *
     * @return Entry[]
     */
    public function getPublushedEntries(bool $publushed, string $type)
    {
        return $this->getPublishedEntriesQueryBuilder($publushed, $type)
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\Entry;
use App\Form\EntryType;
use App\Manager\EntryManager;
use App\Repository\EntryRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class EntryController extends AbstractController
{
    /**
     * @Route("/entry", name="entry_index", methods={"GET"})
     *
     * @param EntryRepository $entryRepository
     *
     * @return Response
     */
    public function index(EntryRepository $entryRepository): Response
    {
        return $this->render('admin/entry/index.html.twig', [
            'entries' => $entryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/entry/new", name="entry_new", methods={"GET","POST"})
     *
     * @param Request      $request
     * @param EntryManager $manager
     *
     * @return Response
     *
     * @throws Exception
     */
    public function new(Request $request, EntryManager $manager): Response
    {
        $entry = new Entry();
        $entry->setUser($this->getUser());

        $form = $this->createForm(EntryType::class, $entry);
        $form->handleRequest($request);

        if ($manager->processForm($form, $entry)) {
            return $this->redirectToRoute('entry_index');
        }

        return $this->render('admin/entry/new.html.twig', [
            'entry' => $entry,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/entry/{id}", name="entry_show", methods={"GET"})
     *
     * @param Entry $entry
     *
     * @return Response
     */
    public function show(Entry $entry): Response
    {
        if (!$entry->getPublished() && !$this->getUser()) {
            return new RedirectResponse($this->generateUrl('entry_index'));
        }

        return $this->render('admin/entry/show.html.twig', [
            'entry' => $entry,
        ]);
    }

    /**
     * @Route("/entry/{id}/edit", name="entry_edit", methods={"GET","POST"})
     *
     * @param Request      $request
     * @param EntryManager $manager
     * @param Entry        $entry
     *
     * @return Response
     *
     * @throws Exception
     */
    public function edit(Request $request, EntryManager $manager, Entry $entry): Response
    {
        $form = $this->createForm(EntryType::class, $entry);
        $form->handleRequest($request);

        if ($manager->processForm($form, $entry)) {
            return $this->redirectToRoute('entry_index');
        }

        return $this->render('admin/entry/edit.html.twig', [
            'entry' => $entry,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/entry/{id}", name="entry_delete", methods={"DELETE"})
     *
     * @param Request      $request
     * @param EntryManager $manager
     * @param Entry        $entry
     *
     * @return Response
     *
     * @throws Exception
     */
    public function delete(Request $request, EntryManager $manager, Entry $entry): Response
    {
        if ($this->isCsrfTokenValid('delete'.$entry->getId(), $request->request->get('_token'))) {
            $manager->removeEntry($entry);
        }

        return $this->redirectToRoute('entry_index');
    }
}

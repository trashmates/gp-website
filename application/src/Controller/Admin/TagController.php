<?php

namespace App\Controller\Admin;

use App\Entity\Tag;
use App\Form\TagType;
use App\Manager\TagManager;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/tag", name="admin.tag.")
 */
class TagController extends AbstractController
{
    /**
     * @var TagManager
     */
    private $manager;

    /**
     * @var TagRepository
     */
    private $repository;

    public function __construct(TagManager $manager, TagRepository $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index()
    {
        $tags = $this->repository->findAll();

        return $this->render('admin/tag/index.html.twig', [
            'tags' => $tags,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function new(Request $request): Response
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);

        if ($this->manager->processForm($form, $request)) {
            return $this->redirectToRoute('admin.tag.index');
        }

        return $this->render('admin/tag/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param Tag     $tag
     *
     * @return Response
     */
    public function edit(Request $request, Tag $tag): Response
    {
        $form = $this->createForm(TagType::class, $tag);

        if ($this->manager->processForm($form, $request)) {
            return $this->redirectToRoute('admin.tag.index');
        }

        return $this->render('admin/tag/edit.html.twig', [
            'form' => $form->createView(),
            'tag' => $tag,
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     *
     * @param Tag $tag
     *
     * @return RedirectResponse
     */
    public function delete(Tag $tag): Response
    {
        $this->manager->remove($tag);

        return $this->redirectToRoute('admin.tag.index');
    }
}

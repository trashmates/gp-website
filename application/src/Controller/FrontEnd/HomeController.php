<?php

namespace App\Controller\FrontEnd;

use App\Entity\Entry;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="fe.home.index")
     *
     * @return Response
     */
    public function index()
    {
        return $this->render('frontend/home/index.html.twig', [
            'comptesRendus' => $this->manager->getRepository(Entry::class)->getPublushedEntries(true, 'Compte Rendu'),
            'members' => $this->manager->getRepository(Entry::class)->getPublushedEntries(true, 'Équipe'),
            'evolutions' => $this->manager->getRepository(Entry::class)->getPublushedEntries(true, 'Évolution'),
        ]);
    }

    /**
     * @Route("/references", name="fe.home.references")
     *
     * @param ObjectManager $this->manager
     *
     * @return Response
     */
    public function references(): Response
    {
        $references = $this->manager->getRepository(Entry::class)
            ->getPublushedEntries(true, 'Reference')
        ;

        return $this->render('frontend/home/references.html.twig', [
            'references' => $references,
        ]);
    }

    /**
     * @Route("/contacts", name="fe.home.contacts")
     *
     * @return Response
     */
    public function contacts()
    {
        return $this->render('frontend/home/contacts.html.twig');
    }

    /**
     * @Route("/journaux", name="fr.home.entries")
     *
     * @return Response
     */
    public function journaux(): Response
    {
        $entries = $this->manager->getRepository(Entry::class)
            ->getPublushedEntries(true, 'Journal')
        ;

        $users = $this->manager->getRepository(User::class)->findAll();

        return $this->render('frontend/home/journaux.html.twig', [
            'entries' => $entries,
            'users' => $users,
        ]);
    }

    /**
     * @Route("/journal/{id}", name="fr.home.entries.entry")
     *
     * @param string $id
     *
     * @return Response
     */
    public function entry(string $id): Response
    {
        $entry = $this->manager->getRepository(Entry::class)->find($id);

        return $this->render('frontend/home/_partials/entry.html.twig', [
            'entry' => $entry,
        ]);
    }

    /**
     * @Route("/store", name="fr.store.index")
     *
     * @return Response
     */
    public function store(): Response
    {
        return $this->render('frontend/store/index.html.twig');
    }
}

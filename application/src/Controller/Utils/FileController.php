<?php

namespace App\Controller\Utils;

use App\Repository\FileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends AbstractController
{
    /**
     * @Route("/admin/files", name="utils.uploads.index")
     *
     * @param FileRepository $fileRepository
     *
     * @return Response
     */
    public function index(FileRepository $fileRepository): Response
    {
        return $this->render('admin/uploads/index.html.twig', [
            'files' => $fileRepository->findAll(),
        ]);
    }

    /**
     * @Route("/uploads/{id}", name="utils.uploads.show")
     *
     * @param string         $id
     * @param FileRepository $fileRepository
     *
     * @return Response
     */
    public function show(string $id, FileRepository $fileRepository): Response
    {
        $response = new Response('Not Found', 404);

        $file = $fileRepository->find($id);
        if (null !== $file) {
            try {
                $response = new BinaryFileResponse($file->getFile());
            } catch (\Exception $exception) {
                return $response;
            }

            $response->headers->set('Content-Type', $file->getFile()->getMimeType());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file->getFile()->getFilename());
        }

        return $response;
    }
}

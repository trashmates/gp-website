<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191020125021 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO entry_type(name, created_at, updated_at) VALUES ("Équipe", NOW(), NOW()), ("Évolution", NOW(), NOW())');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM entry_type WHERE name IN ("Équipe", "Évolution")');
    }
}

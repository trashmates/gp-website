<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191019125311 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entry_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('INSERT INTO entry_type (name) VALUES ("Journal"), ("Reference"), ("Contact"), ("Compte rendu")');
        $this->addSql('ALTER TABLE entry ADD type_id INT NOT NULL');
        $this->addSql('UPDATE entry SET type_id = 1');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D70C54C8C93 FOREIGN KEY (type_id) REFERENCES entry_type (id)');
        $this->addSql('CREATE INDEX IDX_2B219D70C54C8C93 ON entry (type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D70C54C8C93');
        $this->addSql('DROP TABLE entry_type');
        $this->addSql('DROP INDEX IDX_2B219D70C54C8C93 ON entry');
        $this->addSql('ALTER TABLE entry DROP type_id');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191013132227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entry ADD thumbnail_id INT DEFAULT NULL, DROP thumbnail_path');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D70FDFF2E92 FOREIGN KEY (thumbnail_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2B219D70FDFF2E92 ON entry (thumbnail_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D70FDFF2E92');
        $this->addSql('DROP INDEX UNIQ_2B219D70FDFF2E92 ON entry');
        $this->addSql('ALTER TABLE entry ADD thumbnail_path VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP thumbnail_id');
    }
}
